// inputs
export const purchase_price = ({ purchase_price_in }) => purchase_price_in;
export const sales_price = ({ sales_price_in }) => sales_price_in;
export const units = ({ units_in }) => units_in;
export const fixed_expenses = ({ fixed_expenses_in }) => fixed_expenses_in;

// formulae
export const sales = ({ units_in, sales_price_in }) => units({ units_in }) * sales_price({ sales_price_in });
export const purchases = ({ units_in, purchase_price_in }) => units({ units_in }) * purchase_price({ purchase_price_in });
export const profit = ({ units_in, sales_price_in, purchase_price_in, fixed_expenses_in }) => sales({ units_in, sales_price_in }) - purchases({ units_in, purchase_price_in }) - fixed_expenses({ fixed_expenses_in });
// inputs
export const purchase_price = () => purchase_price_in;
export const sales_price = () => sales_price_in;
export const units = () => units_in;
export const fixed_expenses = () => fixed_expenses_in;

// formulae
export const sales = () => units() * sales_price();
export const purchases = () => units() * purchase_price();
export const profit = () => sales() - purchases() - fixed_expenses();
